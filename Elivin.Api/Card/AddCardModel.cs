﻿namespace Elivin.Api.Card
{
    public class AddCardModel
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
    }
}
