﻿using System;

namespace Elivin.Api.Card
{
    public class CardModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
    }
}
