﻿using Elivin.Api.Card;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Elivin.Api.Controllers
{
    [Route("api/cards")]
    [ApiController]
    public class CardController : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CardModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var respose = new List<CardModel>()
            {
                new CardModel()
                {
                    Title = "Test",
                    Category = "New",
                    Price = 1.2m,
                    Id = Guid.NewGuid()
                }
            };

            return Ok(respose);
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> Add(CardModel card)
        {
            return Created("", null);
        }

        [HttpPatch("{cardId}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Update(Guid cardId)
        {
            return Ok();
        }

        [HttpDelete("{cardId}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(Guid cardId)
        {
            return NoContent();
        }
    }
}